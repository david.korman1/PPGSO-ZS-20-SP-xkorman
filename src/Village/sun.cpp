#include "sun.h"


Sun::Sun(float x, float y, float z) {

    position = glm::vec3{x, y, z};

}

void Sun::render(Scene &scene) {

    shader.use();

    if(scene.hasCamera){
        shader.setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
        shader.setUniform("ViewMatrix", scene.camera->viewMatrix);
    }

    shader.setUniform("OverallColor", color);
    shader.setUniform("ModelMatrix", modelMatrix);
    shader.setUniform("Texture", texture);
    mesh.render();
}

bool Sun::update(Scene &scene, float time) {
    position = {sin(time/10)*200, cos(time/10)*150, 0};
    generateModelMatrix();
    return true;
}