#pragma once

#ifndef PPGSO_OBJECT_H
#define PPGSO_OBJECT_H

#include <glm/glm.hpp>
#include "../../shaders/diffuse_vert_glsl.h"
#include "../../shaders/diffuse_frag_glsl.h"
#include "../../shaders/texture_vert_glsl.h"
#include "../../shaders/texture_frag_glsl.h"
#include "../../shaders/color_vert_glsl.h"
#include "../../shaders/color_frag_glsl.h"
#include "../../ppgso/ppgso.h"

class Scene;

class Object {

public:
    Object() = default;
    Object(const Object&) = default;
    Object(Object&&) = default;
    virtual ~Object() {};

    glm::vec3 position{0,0,0};
    glm::vec3 rotation{0,0,0};
    glm::vec3 scale{1,1,1};
    glm::vec3 color{1,0,0};
    glm::mat4 viewMatrix{1};
    glm::mat4 projectionMatrix{1};
    glm::mat4 modelMatrix{1};

    bool destroy = false;

    virtual bool update(Scene &scene, float dt) = 0;
    virtual void render(Scene &scene) = 0;

protected:
    void generateModelMatrix();
};


#endif //PPGSO_OBJECT_H
