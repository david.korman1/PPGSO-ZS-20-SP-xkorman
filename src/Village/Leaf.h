#ifndef LEAF
#define LEAF
#pragma once

#include "Object.h"
#include "Scene.h"

class Leaf final : public Object {

private:
    glm::vec3 changer;
    const std::string planeMesh = "../Resources/leaf.obj";
    const std::string planeTexture = "../Resources/leaf.bmp";

    ppgso::Shader shader = ppgso::Shader(texture_vert_glsl, texture_frag_glsl);
    ppgso::Mesh mesh = ppgso::Mesh(planeMesh);
    ppgso::Texture texture = ppgso::Texture(ppgso::image::loadBMP(planeTexture));


public:
    Leaf(float x, float y, float z);
    ~Leaf(){
        mesh.~Mesh();
        shader.~Shader();
        texture.~Texture();
    };

    bool update(Scene &scene, float time);
    void render(Scene &scene);

};
#endif
