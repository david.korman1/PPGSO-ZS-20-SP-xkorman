#ifndef TREE
#define TREE
#include "Object.h"
#include "Scene.h"
#include "../../shaders/phong_frag_glsl.h"
#include "../../shaders/phong_vert_glsl.h"

class Tree final : public Object {

private:
    const std::string planeMesh = "../Resources/tree1.obj";
    const std::string planeTexture = "../Resources/tree.bmp";

    ppgso::Shader shader = ppgso::Shader(phong_vert_glsl, phong_frag_glsl);
    ppgso::Mesh mesh = ppgso::Mesh(planeMesh);
    ppgso::Texture texture = ppgso::Texture(ppgso::image::loadBMP(planeTexture));


public:
    Tree(float x, float y, float z);
    ~Tree(){
        mesh.~Mesh();
        shader.~Shader();
        texture.~Texture();
    };

    bool update(Scene &scene, float time);
    void render(Scene &scene);

};
#endif