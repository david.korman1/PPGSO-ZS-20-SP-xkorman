#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include "Object.h"

void Object::generateModelMatrix() {

    modelMatrix = glm::translate(glm::mat4(1.0f), position)
                  * glm::orientate4(rotation)
                  * glm::scale(glm::mat4(1.0f), scale);
}