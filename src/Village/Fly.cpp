#include "Fly.h"

Fly::Fly(float x, float y, float z) {
    position = glm::vec3{x, y, z};
}

void Fly::render(Scene &scene) {

    shader.use();

    shader.setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader.setUniform("ViewMatrix", scene.camera->viewMatrix);
    shader.setUniform("viewPos", scene.camera->position);

    shader.setUniform("dirLight.ambient", scene.ambient);
    shader.setUniform("dirLight.diffuse", scene.diffuse);
    shader.setUniform("dirLight.specular", scene.specular);

    shader.setUniform("dirLight.direction", scene.lightDirection);
    shader.setUniform("dirLight.color", scene.lightColor);

    glm::vec3 material_ambient = {1,1,1};
    glm::vec3 material_diffuse = {.5f,.5f,.5f};
    glm::vec3 material_specular = {.8f,.8f,.8f};
    float material_shininess = 128.f;
    shader.setUniform("material.ambient", material_ambient);
    shader.setUniform("material.diffuse", material_diffuse);
    shader.setUniform("material.specular", material_specular);
    shader.setUniform("material.shininess", material_shininess);

    shader.setUniform("pointLight.position", scene.lampLight.position);
    shader.setUniform("pointLight.ambient", scene.lampLight.ambient);
    shader.setUniform("pointLight.diffuse", scene.lampLight.diffuse);
    shader.setUniform("pointLight.specular", scene.lampLight.specular);
    shader.setUniform("pointLight.constant", scene.lampLight.constant);
    shader.setUniform("pointLight.linear", scene.lampLight.linear);
    shader.setUniform("pointLight.quadratic", scene.lampLight.quadratic);

    shader.setUniform("ModelMatrix", modelMatrix);
    shader.setUniform("Texture", texture);
    mesh.render();
}

bool Fly::update(Scene &scene, float time) {
    generateModelMatrix();

//    lampLight->update(scene, time, modelMatrix);
    return true;
}

bool Fly::update(Scene &scene, float time, glm::mat4 parentModel, glm::mat4 oldParent) {
    float temp;
    if (animation && returnAnimation){
        glm::vec3 dirToPlayer;
        dirToPlayer.x = position.x - lastPost.x;
        dirToPlayer.z = position.z - lastPost.z;

        // get the player angle on the y axis
        rotation.z = static_cast<float>(atan2(-dirToPlayer.z, dirToPlayer.x)) - ppgso::PI/2;

        // move the player towards the enemy
        position.x += 10 * time * static_cast<float>(sin(rotation.z));
        position.z += 10 * time * static_cast<float>(cos(rotation.z));
        position.y = 4;
        if (distance(position, scene.lampLight.position) < scale.y*30) {
            animation = false;
            scale = {1,1,1};
            generateModelMatrix();
            modelMatrix = parentModel * modelMatrix;
            return true;
        }
        generateModelMatrix();
        return true;
    }

    if (distance(scene.lampLight.position, scene.playerPosition) < scale.y*30) {
        if (!animation && !returnAnimation) {
            animation = true;
            lastPost = scene.lampLight.position;
            position = scene.lampLight.position;
            position.y = 4;
            scale = {.1,.1,.1};
        }
    }
    // ku mne
    if (animation) {
        if (powf((position.x - scene.playerPosition.x),2) < powf(.1f,2) &&
                powf((position.z - scene.playerPosition.z),2) < powf(.1f,2)) {
            returnAnimation = true;
        }
        glm::vec3 dirToPlayer;
        dirToPlayer.x = position.x - scene.playerPosition.x;
        dirToPlayer.z = position.z - scene.playerPosition.z;

        // get the player angle on the y axis
        rotation.z = static_cast<float>(atan2(-dirToPlayer.z, dirToPlayer.x)) - ppgso::PI / 2;

        // move the player towards the enemy
        position.x += 10 * time * static_cast<float>(sin(rotation.z));
        position.z += 10 * time * static_cast<float>(cos(rotation.z));
        position.y = 5;
        generateModelMatrix();
//        modelMatrix = modelMatrix;
    } else {
        auto timeOrigin = glfwGetTime();
        // okolo svetla
        if (sin(temp * timeOrigin) < 0) {
            rotation = {0, 0, 0};
        } else {
            rotation = {0, 0, ppgso::PI};
        }
        position = glm::vec3{7 * sin(timeOrigin), sin(10 * timeOrigin), 7 * cos(timeOrigin)};
        position.y -= 15;
        generateModelMatrix();
        modelMatrix = parentModel * modelMatrix;
    }


    return true;
}


