#pragma once
#ifndef PPGSO_CAMERA_H
#define PPGSO_CAMERA_H

#include "Object.h"

class Scene;

class Cam : public Object {

public:
    glm::vec3 up{0,1,0};

    glm::vec3 playerPosition{0,0,0};
    float playerYaw = 0;

    float pitch = 5;
    float yaw = 0;
    float zoom = 15.0;
    float zoomMax = 50.0;
    float zoomMin = 10.0;

    bool overView = false;
    bool animation = false;
    std::vector<glm::vec3> keyframes;

    Cam(float fow, float ratio, float near, float far);
    bool update(Scene &scene, float dt) override;
    void render(Scene &scene) override;
};


#endif //PPGSO_CAMERA_H
