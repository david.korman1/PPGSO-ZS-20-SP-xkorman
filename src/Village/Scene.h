#ifndef PPGSO_SCENE_H
#define PPGSO_SCENE_H


#include <memory>
#include <map>
#include <list>
#include "Object.h"
#include "Camera.h"

class Player;

struct Mouse{
    double posX;
    double posY;
    double lastPosX = 0;
    double lastPosY = 0;
    double scrollX;
    double scrollY;
    std::map<int, int> button;
};

class Scene{

public:
    std::vector<Object*> objects;
    std::vector<Object*> leafs;
    std::vector<Object*> drops;
    std::vector<Object*> shadows;

    std::vector<glm::vec3> colors = {{1,1,1}, {0,0,0}, {1,1,0}, {0,1,1}};
    size_t color_index = 0;

    glm::vec3 lightColor = colors[color_index];
    glm::vec3 ambient = {1.f,1.0f,1.0f};
    glm::vec3 diffuse = {1.f,1.f,1.f};
    glm::vec3 specular = {1,1,1};

    struct PointLight{
        glm::vec3 position = {0, 0, 115};
        glm::vec3 color;

        float constant = 1;
        float linear = 0.045f;
        float quadratic = 0.0075f;

        glm::vec3 ambient = {1.f, 1.f, 0.f};
        glm::vec3 diffuse = {0.2f, 0.2f, 0.2f};
        glm::vec3 specular = {1.0f, 1.0f, 1.0f};
    };

    PointLight lampLight;

    Cam *camera;
    Mouse mouse;

    glm::vec3 gravity = {0,-10,0};
    glm::vec3 wind = {.3,.1,.7};

    glm::vec3 playerPosition;

    std::map<int, int> keyboard;
    bool hasCamera = false;
    bool menu = true;
    glm::vec3 lightDirection{0, 100, 0};
    void update(float time, float timeOrigin);
    void render();
    void useCamera(Cam *camera);
};


#endif //PPGSO_SCENE_H
