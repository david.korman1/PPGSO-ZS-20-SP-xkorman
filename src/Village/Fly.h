#pragma once
#include "Object.h"
#include "Scene.h"
#include "../../shaders/phong_frag_glsl.h"
#include "../../shaders/phong_vert_glsl.h"

class Fly final : public Object {

private:
    const std::string planeMesh = "../Resources/fly.obj";
    const std::string planeTexture = "../Resources/fly.bmp";

    ppgso::Shader shader = ppgso::Shader(phong_vert_glsl, phong_frag_glsl);
    ppgso::Mesh mesh = ppgso::Mesh(planeMesh);
    ppgso::Texture texture = ppgso::Texture(ppgso::image::loadBMP(planeTexture));

    glm::vec3 lastPost;
public:
    Fly(float x, float y, float z);
//    Fly(glm::vec3 vec);

    ~Fly(){
        mesh.~Mesh();
        shader.~Shader();
        texture.~Texture();
    };

    bool animation = false;
    bool returnAnimation = false;

    bool update(Scene &scene, float time);
    void render(Scene &scene);

    bool update(Scene &scene, float time, glm::mat4 parentModel, glm::mat4 oldParent);

};
