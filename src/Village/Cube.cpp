#include "Cube.h"

Cube::Cube(float x, float y, float z) {
    position = glm::vec3{x, y, z};
}

void Cube::render(Scene &scene) {

    shader.use();

    if(scene.hasCamera){
        shader.setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
        shader.setUniform("ViewMatrix", scene.camera->viewMatrix);
    }

    shader.setUniform("OverallColor", color);
    shader.setUniform("ModelMatrix", modelMatrix);
    shader.setUniform("Texture", texture);
    mesh.render();
}

bool Cube::update(Scene &scene, float time) {
    generateModelMatrix();
    return true;
}