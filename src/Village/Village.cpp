#include <regex>
#include "Player.h"
#include "House.h"
#include "Camera.h"
#include "Scene.h"
#include "Background.h"
#include "Cube.h"
#include "Plate.h"
#include "Leaf.h"
#include "sun.h"
#include "Tree.h"
#include "Lamp.h"

const unsigned int H_SIZE = 1080;
const unsigned int W_SIZE = 1920;

class SceneWindow : public ppgso::Window {

    Scene scene;
    Background *background;
    Cube *axisX, *axisY, *axisZ, *helpDesk;
    Sun *sun;
    Plate *terrain;
    Player *player;
    Cam *camera;
    House *house;
    Leaf *leaf;
    Tree *tree;
    Lamp *lamp;
    void initScene(){
        scene.objects.clear();
        scene.leafs.clear();
        background = new Background();
        scene.objects.push_back(background);
        sun = new Sun(25,25,0);
        scene.objects.push_back(sun);
        axisX = new Cube(0,0,0);
        axisY = new Cube(0,0,0);
        axisZ = new Cube(0,0,0);
        axisX->color = {1,0,0};
        axisY->color = {0,1,0};
        axisZ->color = {0,0,1};
        axisX->scale = {400, .1f, .1f};
        axisY->scale = {.1f, 100, .1f};
        axisZ->scale = {.1f, .1f, 400};
        terrain = new Plate(0,0,0);
        terrain->scale = {200, 1, 200};
//        scene.objects.push_back(axisX);
//        scene.objects.push_back(axisY);
//        scene.objects.push_back(axisZ);

        scene.objects.push_back(terrain);
        player = new Player(0,0,0);
        player->scale = {.02f,.02f,.02f};
        scene.objects.push_back(player);
        float x,z, random;
        for (size_t i = 0; i < 10; i++) {
            x = static_cast<float>(rand() % 200 - 100);
            z = static_cast<float>(rand() % 200 - 100);
//            random = rand() % 3;
            house = new House(x, 0, z);
            house->rotation = {-ppgso::PI/2, 0, 0};
            scene.objects.push_back(house);
            house = new House(x, 0.1f ,z, true, "../Resources/h2_shadow.bmp");
            house->rotation = {0,0,ppgso::PI/2};
            house->scale = {1,0,1};

            scene.shadows.push_back(house);

        }

        for (size_t i = 0; i < 10; i++) {
            x = static_cast<float>(rand() % 200 - 100);
            z = static_cast<float>(rand() % 200 - 100);
            random = rand() % 10;
            tree = new Tree(x,0,z);
            tree->scale = {.5f,.5f,.5f};
            tree->rotation = {-ppgso::PI/2, random, 0};
            scene.objects.push_back(tree);
        }

        lamp = new Lamp({45,0,45});
        lamp->scale = {.1f,.1f,.1f};
        scene.objects.push_back(lamp);



        camera = new Cam(30.0f, 2.0f, 1.f, 300.0f);
        scene.useCamera(camera);
    };

    void initMenu() {
        scene.objects.clear();
        background = new Background();
        scene.objects.push_back(background);

        helpDesk = new Cube(0,10,25);
        helpDesk->scale = {5,5,5};
        helpDesk->rotation.y = -ppgso::PI/2;
        scene.objects.push_back(helpDesk);
        axisX = new Cube(0,0,0);
        axisY = new Cube(0,0,0);
        axisZ = new Cube(0,0,0);
        axisX->color = {1,0,0};
        axisY->color = {0,1,0};
        axisZ->color = {0,0,1};
        axisX->scale = {400, .1f, .1f};
        axisY->scale = {.1f, 100, .1f};
        axisZ->scale = {.1f, .1f, 400};
        terrain = new Plate(0,0,0);
        terrain->scale = {200, 1, 200};
//        scene.objects.push_back(axisX);
//        scene.objects.push_back(axisY);
//        scene.objects.push_back(axisZ);
        house = new House(-10, 0 ,0);
        house->rotation = {-ppgso::PI/2, 2.5, 0};

        tree = new Tree(10, 0, 0);
        tree->scale = {1,1,1};
        tree->rotation = {-ppgso::PI/2, 0, 0};
        scene.objects.push_back(tree);

        scene.objects.push_back(terrain);
        scene.objects.push_back(house);
        scene.objects.push_back(tree);

//        for (size_t i = 0; i < 3; i++) {
//            leaf = new Leaf (static_cast<float>(i), 3, 0);
//            leaf->scale = {10,10,10};
//            scene.leafs.push_back(leaf);
//        }

//        lamp = new Lamp({0,0,115});
//        lamp->scale = {.2f,.2f,.2f};
//        scene.objects.push_back(lamp);

        scene.lightDirection = {0, 50, -25};

        camera = new Cam(30.0f, 2.0f, 1.f, 300.0f);
        camera->pitch = -5;
        camera->playerPosition = {0, 5, 0};
        scene.useCamera(camera);
    }

public:

    SceneWindow() : Window("Village | DKspace", W_SIZE, H_SIZE) {

        glfwSetInputMode(window, GLFW_STICKY_KEYS, 1);
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

//        glEnable(GL_CULL_FACE);
        glFrontFace(GL_CCW);
        glCullFace(GL_BACK);


        glEnable(GL_LIGHTING);
        glEnable(GL_COLOR_MATERIAL);

        initMenu();

    }

    void onKey(int key, int scanCode, int action, int mods) override {
        scene.keyboard[key] = action;
//        std::cout << "ok\n" << key << " " << action;
    }

    void onScroll(double cursorX, double cursorY) {
        scene.mouse.scrollX = cursorX;
        scene.mouse.scrollY = cursorY;
    }

    void onMouseButton(int button, int action, int mods) override {
        if(button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS){
            glfwGetCursorPos(window, &scene.mouse.lastPosX, &scene.mouse.lastPosY);
        }
        scene.mouse.button[button] = action;
    }

    void onCursorPos(double cursorX, double cursorY) override {
        scene.mouse.posX = cursorX;
        scene.mouse.posY = cursorY;
    }

    void onIdle() override {

        static auto time = (float) glfwGetTime();

        // Compute time delta
        float dt = (float) glfwGetTime() - time;

        time = (float) glfwGetTime();

        glClearColor(.5f, .5f, .5f, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if (scene.menu) {
            scene.update(dt, time);
            if (!scene.menu) {
                initScene();
            }
        } else {
            scene.update(dt, time);
        }
        scene.render();
    }

};

int main() {
    // Initialize our window
    SceneWindow window;

    // Main execution loop
    while (window.pollEvents()) {}

    return EXIT_SUCCESS;
}