#ifndef PPGSO_BACKGROUND_H
#define PPGSO_BACKGROUND_H

#include "Object.h"

class Scene;

class Background final : public Object{

private:
    const std::string terrainMesh = "../Resources/quad.obj";
    const std::string terrainTexture = "../Resources/sky.bmp";

    ppgso::Shader shader = ppgso::Shader(texture_vert_glsl, texture_frag_glsl);
    ppgso::Mesh mesh = terrainMesh;
    ppgso::Texture texture = ppgso::image::loadBMP(terrainTexture);

    glm::vec2 textureOffset;
public:
    Background();

    bool update(Scene &scene, float dt) override;
    void render(Scene &scene) override;

};

#endif //PPGSO_BACKGROUND_H
