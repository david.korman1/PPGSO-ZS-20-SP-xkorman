#include "Plate.h"

Plate::Plate(float x, float y, float z) {
    position = glm::vec3{x, y, z};
}

void Plate::render(Scene &scene) {

    shader.use();

    shader.setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader.setUniform("ViewMatrix", scene.camera->viewMatrix);
    shader.setUniform("viewPos", scene.camera->position);

    shader.setUniform("dirLight.ambient", scene.ambient);
    shader.setUniform("dirLight.diffuse", scene.diffuse);
    shader.setUniform("dirLight.specular", scene.specular);

    shader.setUniform("dirLight.direction", scene.lightDirection);
    shader.setUniform("dirLight.color", scene.lightColor);

    glm::vec3 material_ambient = {.1f,.1,.1};
    glm::vec3 material_diffuse = {.5f,.5f,.5f};
    glm::vec3 material_specular = {.2f,.2f,.2f};
    float material_shininess = 4.f;
    shader.setUniform("material.ambient", material_ambient);
    shader.setUniform("material.diffuse", material_diffuse);
    shader.setUniform("material.specular", material_specular);
    shader.setUniform("material.shininess", material_shininess);

    shader.setUniform("pointLight.position", scene.lampLight.position);
    shader.setUniform("pointLight.ambient", scene.lampLight.ambient);
    shader.setUniform("pointLight.diffuse", scene.lampLight.diffuse);
    shader.setUniform("pointLight.specular", scene.lampLight.specular);
    shader.setUniform("pointLight.constant", scene.lampLight.constant);
    shader.setUniform("pointLight.linear", scene.lampLight.linear);
    shader.setUniform("pointLight.quadratic", scene.lampLight.quadratic);

    shader.setUniform("ModelMatrix", modelMatrix);
    shader.setUniform("Texture", texture);
    mesh.render();
}

bool Plate::update(Scene &scene, float time) {
    generateModelMatrix();
    return true;
}
