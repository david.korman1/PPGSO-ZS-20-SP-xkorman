#pragma once
#include "Object.h"
#include "Scene.h"
#include "Lamp_light.h"
#include "../../shaders/phong_frag_glsl.h"
#include "../../shaders/phong_vert_glsl.h"

class Lamp final : public Object {

private:
    const std::string planeMesh = "../Resources/lamp1.obj";
    const std::string planeTexture = "../Resources/lamp.bmp";

    ppgso::Shader shader = ppgso::Shader(phong_vert_glsl, phong_frag_glsl);
    ppgso::Mesh mesh = ppgso::Mesh(planeMesh);
    ppgso::Texture texture = ppgso::Texture(ppgso::image::loadBMP(planeTexture));

    LampL *lampas;
public:
    Lamp(float x, float y, float z);
    ~Lamp(){
        mesh.~Mesh();
        shader.~Shader();
        texture.~Texture();
    };

    bool update(Scene &scene, float time);
    void render(Scene &scene);

};
