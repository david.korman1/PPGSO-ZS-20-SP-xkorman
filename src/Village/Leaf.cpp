#include "Leaf.h"


Leaf::Leaf(float x, float y, float z) {
    position = glm::vec3{x, y, z};
}

void Leaf::render(Scene &scene) {

    shader.use();

    if(scene.hasCamera){
        shader.setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
        shader.setUniform("ViewMatrix", scene.camera->viewMatrix);
    }

    shader.setUniform("OverallColor", color);
    shader.setUniform("ModelMatrix", modelMatrix);
    shader.setUniform("Texture", texture);
    mesh.render();
}

bool Leaf::update(Scene &scene, float time) {
    if (!scene.menu) {
        int x = glm::linearRand(0, 100);
        if (x == 1) {
            changer = glm::vec3{static_cast <float> (rand()) / static_cast <float> (RAND_MAX),
                             static_cast <float> (rand()) / static_cast <float> (RAND_MAX),
                             static_cast <float> (rand()) / static_cast <float> (RAND_MAX)};
        }
        if (position.y == 0) {
            position.y += changer.y * time * scene.wind.y;
        } else {
            position += changer * time + time * scene.wind;
            position += scene.gravity * (time/20);
//            position.y += gravity.y * time / 20;
        }

        if (position.y > 5) {
            changer.y = -.3f;
        }
        if (position.y < 0) {
            position.y = 0;
            changer.y = static_cast<float>(glm::linearRand(0, 1));
        }
        if (position.y > 0.1f) {
            int randomize = glm::linearRand(10, 30);
            rotation.x += changer.x / static_cast<float>(randomize);
            rotation.z += changer.z / static_cast<float>(randomize);
        } else {
            rotation = {0, 0, 0};
        }
    }

    generateModelMatrix();

    return true;
}