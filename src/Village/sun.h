#ifndef SUN
#define SUN
#pragma once

#include "Object.h"
#include "Scene.h"

class Sun final : public Object {

private:
    const std::string planeMesh = "../Resources/Sun.obj";
    const std::string planeTexture = "../Resources/Sun.bmp";

    ppgso::Shader shader = ppgso::Shader(texture_vert_glsl, texture_frag_glsl);
    ppgso::Mesh mesh = ppgso::Mesh(planeMesh);
    ppgso::Texture texture = ppgso::Texture(ppgso::image::loadBMP(planeTexture));


public:
    Sun(float x, float y, float z);
    ~Sun(){
        mesh.~Mesh();
        shader.~Shader();
        texture.~Texture();
    };

    bool update(Scene &scene, float time);
    void render(Scene &scene);

};
#endif
