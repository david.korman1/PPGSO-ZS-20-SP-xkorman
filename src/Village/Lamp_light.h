#pragma once
#include "Object.h"
#include "Scene.h"
#include "Fly.h"
#include "../../shaders/phong_frag_glsl.h"
#include "../../shaders/phong_vert_glsl.h"

class LampL final : public Object {

private:
    const std::string planeMesh = "../Resources/lamp2.obj";
    const std::string planeTexture = "../Resources/lamp.bmp";

    ppgso::Shader shader = ppgso::Shader(phong_vert_glsl, phong_frag_glsl);
    ppgso::Mesh mesh = ppgso::Mesh(planeMesh);
    ppgso::Texture texture = ppgso::Texture(ppgso::image::loadBMP(planeTexture));

    Fly *fly;
public:
    LampL(float x, float y, float z);
//    LampL(glm::vec3 vec);
    glm::mat4 oldParent;

    ~LampL(){
        mesh.~Mesh();
        shader.~Shader();
        texture.~Texture();
    };

    bool update(Scene &scene, float time);
    void render(Scene &scene);

    bool update(Scene &scene, float time, glm::mat4 parentModel);

};
