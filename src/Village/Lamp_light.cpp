#include <glm/gtx/euler_angles.hpp>
#include "Lamp_light.h"

LampL::LampL(float x, float y, float z) {
    fly = new Fly(0,0,0);
}

void LampL::render(Scene &scene) {

    shader.use();

    shader.setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader.setUniform("ViewMatrix", scene.camera->viewMatrix);
    shader.setUniform("viewPos", scene.camera->position);

    shader.setUniform("dirLight.ambient", scene.ambient);
    shader.setUniform("dirLight.diffuse", scene.diffuse);
    shader.setUniform("dirLight.specular", scene.specular);

    shader.setUniform("dirLight.direction", scene.lightDirection);
    shader.setUniform("dirLight.color", scene.lightColor);

    glm::vec3 material_ambient = {1,1,1};
    glm::vec3 material_diffuse = {.5f,.5f,.5f};
    glm::vec3 material_specular = {.8f,.8f,.8f};
    float material_shininess = 128.f;
    shader.setUniform("material.ambient", material_ambient);
    shader.setUniform("material.diffuse", material_diffuse);
    shader.setUniform("material.specular", material_specular);
    shader.setUniform("material.shininess", material_shininess);

    shader.setUniform("pointLight.position", scene.lampLight.position);
    shader.setUniform("pointLight.ambient", scene.lampLight.ambient);
    shader.setUniform("pointLight.diffuse", scene.lampLight.diffuse);
    shader.setUniform("pointLight.specular", scene.lampLight.specular);
    shader.setUniform("pointLight.constant", scene.lampLight.constant);
    shader.setUniform("pointLight.linear", scene.lampLight.linear);
    shader.setUniform("pointLight.quadratic", scene.lampLight.quadratic);

    shader.setUniform("ModelMatrix", modelMatrix);
    shader.setUniform("Texture", texture);
    mesh.render();

    fly->render(scene);
}

bool LampL::update(Scene &scene, float time) {
    generateModelMatrix();

//    lampLight->update(scene, time, modelMatrix);
    return true;
}

bool LampL::update(Scene &scene, float time, glm::mat4 parentModel) {
    auto dt = (float) glfwGetTime();
    position.y = 50;
    rotation = glm::vec3{ppgso::PI/180 * 20 * sin(dt*3), 0,ppgso::PI/180 * 20 * cos(dt*3)};
    modelMatrix = parentModel * glm::translate(glm::mat4(1.0f), position) * glm::orientate4(rotation);
    fly->update(scene, time, modelMatrix, oldParent);


    return true;
}


