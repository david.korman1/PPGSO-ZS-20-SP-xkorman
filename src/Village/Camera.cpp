#include <thread>
#include <chrono>
#include "Camera.h"
#include "Scene.h"

Cam::Cam(float fow, float ratio, float near, float far) {
    float fowInRad = (ppgso::PI/180.0f) * fow;
    projectionMatrix = glm::perspective(fowInRad, ratio, near, far);
}

void calculateZoom(const float pitch, const float zoom, float *zoomY, float *zoomZ){
    *zoomY = (float) zoom * glm::sin(glm::radians(pitch));
    *zoomZ = (float) zoom * glm::cos(glm::radians(pitch));
}

void calculatePositionOffset(
        const float angle,
        const float zoomY,
        const float zoomZ,
        float *offsetX,
        float *offsetY,
        float *offsetZ){
    *offsetX = (float) zoomZ * glm::sin(glm::radians(angle));
    *offsetY = (float) zoomY;
    *offsetZ = (float) zoomZ * glm::cos(glm::radians(angle));
}

void calculatePosition(
        const float offsetX,
        const float offsetY,
        const float offsetZ,
        const glm::vec3 playerPosition,
        glm::vec3 *position){
    position->x = playerPosition.x - offsetX;
    position->y = playerPosition.y + offsetY;
    position->z = playerPosition.z - offsetZ;
}

bool Cam::update(Scene &scene, float dt) {

    if(zoom < zoomMax && zoom > zoomMin) {
        zoom += scene.mouse.scrollY * dt * 100;
        scene.mouse.scrollY = 0;
    }

    if(zoom >= zoomMax){
        zoom = zoomMax - 1;
    }

    if(zoom <= zoomMin){
        zoom = zoomMin + 1;
    }

    if(scene.keyboard[GLFW_KEY_LEFT_SHIFT]){
        if (!animation) {
            animation = true;
            if (keyframes.empty()) {
                std::vector<glm::vec3> keys;
                size_t PATCH_SIZE;
                keys.emplace_back(200, 25, -200);
                keys.emplace_back(0, 25, 150);
                keys.emplace_back(-50, 25, 0);
                keys.emplace_back(0, 25, -50);
                keys.emplace_back(25, 5, 0);

                for (size_t j = 0; j < keys.size() - 1; ++j) {
                    PATCH_SIZE = 300.f;
                    for (size_t i = 1; i < PATCH_SIZE; i++) {
                        auto f = static_cast<float>(1.f / PATCH_SIZE) * i;
                        glm::vec3 point = glm::lerp(keys[j], keys[j + 1], f);
                        keyframes.push_back(point);
                    }
                }
            }
            projectionMatrix = glm::perspective((ppgso::PI/180.0f) * 30.0f, 2.f, 1.f, 600.0f);
        } else {
            projectionMatrix = glm::perspective((ppgso::PI/180.0f) * 30.0f, 2.f, 1.f, 300.0f);
            animation = false;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
    } else if (!animation) {
        if (scene.mouse.button[GLFW_MOUSE_BUTTON_1]) {
            if (!overView) {
                pitch -= static_cast<float>(scene.mouse.lastPosY - scene.mouse.posY);
                yaw += static_cast<float>(scene.mouse.lastPosX - scene.mouse.posX);
                scene.mouse.lastPosY = scene.mouse.posY;
                scene.mouse.lastPosX = scene.mouse.posX;
            }
        }

        if (scene.keyboard[GLFW_KEY_V]) {
            if (overView) {
                overView = false;
                pitch = 5.0f;
            } else {
                overView = true;
                pitch = 5.0f;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(300));
        }

        float zoomY, zoomZ;
        float offsetX, offsetY, offsetZ;

        calculateZoom(pitch, zoom, &zoomY, &zoomZ);
        calculatePositionOffset(playerYaw + yaw, zoomY, zoomZ, &offsetX, &offsetY, &offsetZ);

        if (overView) {
            calculatePositionOffset(playerYaw + 0, zoomY, zoomZ, &offsetX, &offsetY, &offsetZ);
            calculatePosition(-offsetX, offsetY, -offsetZ, playerPosition, &position);
            viewMatrix = lookAt({playerPosition.x, playerPosition.y + 1, playerPosition.z},
                                glm::vec3{position.x,
                                          1,
                                          position.z},
                                up);
        } else {
            calculatePosition(offsetX, offsetY, offsetZ, playerPosition, &position);
            viewMatrix = lookAt(position, playerPosition, up);
        }
    } else {
        viewMatrix = glm::lookAt(keyframes.back(), playerPosition, up);
        keyframes.pop_back();
        if (keyframes.empty()) {
            projectionMatrix = glm::perspective((ppgso::PI/180.0f) * 30.0f, 2.f, 1.f, 300.0f);
            animation = false;
        }
    }


    return true;
}

void Cam::render(Scene &scene) {
}