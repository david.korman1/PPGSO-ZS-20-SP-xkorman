#include "Rain.h"


Rain::Rain(float x, float y, float z) {
    position = glm::vec3{x, y, z};
}

void Rain::render(Scene &scene) {

    shader.use();

    shader.setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
    shader.setUniform("ViewMatrix", scene.camera->viewMatrix);

    shader.setUniform("OverallColor", color);
    shader.setUniform("ModelMatrix", modelMatrix);
    mesh.render();
}

bool Rain::update(Scene &scene, float time) {
    if (position.y < 0) {
        return false;
    }
    position += scene.gravity*time + scene.wind/10.f;

    generateModelMatrix();

    return true;
}