#pragma once
#ifndef PPGSO_HOUSE_H
#define PPGSO_HOUSE_H

#include "Object.h"
#include "Scene.h"
#include "../../shaders/phong_frag_glsl.h"
#include "../../shaders/phong_vert_glsl.h"

class House final : public Object {

private:
    const std::string planeMesh = "../Resources/h2.obj";
    bool shadow = false;

    ppgso::Shader shader = ppgso::Shader(phong_vert_glsl, phong_frag_glsl);
    ppgso::Mesh mesh = ppgso::Mesh(planeMesh);
    ppgso::Texture texture;


public:
    House(float x, float y, float z);
    House(float x, float y, float z,  bool shadow_, const std::string& planeTexture);
    ~House(){
        mesh.~Mesh();
        shader.~Shader();
        texture.~Texture();
    };

    bool update(Scene &scene, float time);
    void render(Scene &scene);

};


#endif //PPGSO_HOUSE_H
