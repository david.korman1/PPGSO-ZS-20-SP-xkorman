#pragma once
#ifndef PPGSO_PLAYER_H
#define PPGSO_PLAYER_H

#include "Object.h"
#include "Scene.h"

class Player final : public Object{

private:
    const std::string planeMesh = "../Resources/goblin.obj";
    const std::string planeTexture = "../Resources/goblin.bmp";

    ppgso::Shader shader = ppgso::Shader(diffuse_vert_glsl, diffuse_frag_glsl);
    ppgso::Mesh mesh = ppgso::Mesh(planeMesh);
    ppgso::Texture texture = ppgso::Texture(ppgso::image::loadBMP(planeTexture));

    float yaw = 0.0;
    float pitch = 0.0;
    float speed = 50.0f;

    glm::vec3 direction{0, 0, 0};

    void calculateDirection(glm::vec3 *direction);
    void calculateRotation(glm::vec3 *rotation);
    void calculatePosition(glm::vec3 *position, float time);

public:
    Player(float x, float y, float z);
    ~Player(){
        shader.~Shader();
        mesh.~Mesh();
        texture.~Texture();
    }

    bool update(Scene &scene, float time);
    void render(Scene &scene);

};


#endif //PPGSO_PLAYER_H
