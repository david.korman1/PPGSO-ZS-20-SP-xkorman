#include "Player.h"
#include "House.h"
#include "Tree.h"


Player::Player(float x, float y, float z) {
    position = glm::vec3{x, y, z};
}

void Player::render(Scene &scene) {

    shader.use();

    if(scene.hasCamera){
        shader.setUniform("ProjectionMatrix", scene.camera->projectionMatrix);
        shader.setUniform("ViewMatrix", scene.camera->viewMatrix);
    }

    shader.setUniform("LightDirection", scene.lightDirection);

    shader.setUniform("OverallColor", color);
    shader.setUniform("ModelMatrix", modelMatrix);
    shader.setUniform("Texture", texture);
    mesh.render();

}

bool Player::update(Scene &scene, float time) {
    speed = 0;
    if(scene.keyboard[GLFW_KEY_RIGHT]) {
        yaw += 2;
    } else if(scene.keyboard[GLFW_KEY_LEFT]) {
        yaw -= 2;
    }

    if(scene.keyboard[GLFW_KEY_UP]) {
        speed = +10;
    } else if(scene.keyboard[GLFW_KEY_DOWN]) {
        speed = -10;
    }

    calculateRotation(&rotation);
    calculateDirection(&direction);
    calculatePosition(&position, time);

    scene.playerPosition = position;

    scene.camera->playerPosition = position;
    scene.camera->playerYaw = (float) (-1.0) * yaw;

    generateModelMatrix();

    auto iter = scene.objects.begin();
    // Hit detection
    for ( auto& obj : scene.objects ) {

        // Ignore self in scene
        if (obj == this)
            continue;

        auto house = dynamic_cast<House*>(obj);
        auto tree = dynamic_cast<Tree*>(obj);
        if (!house) {
            if (tree && ((distance(position, tree->position) < tree->scale.y*3))) {
                tree->destroy = true;
            }
            iter++;
            continue;
        }
        iter++;
        if (distance(position, house->position) < house->scale.y*3) {
            position.x -= direction.x * speed * time;
            position.z -= direction.z * speed * time;
        }
    }

    return !destroy;
}

void Player::calculateDirection(glm::vec3 *direction){
    direction->z = (float) glm::sin(glm::radians(yaw + 90));
    direction->x = (float) glm::cos(glm::radians(yaw + 90));
    direction->y = (float) glm::sin(glm::radians(pitch));
}

void Player::calculateRotation(glm::vec3 *rotation){
    rotation->z = -1.0f * (float) glm::radians(yaw);
}

void Player::calculatePosition(glm::vec3 *position, float time) {
    position->x += direction.x * speed * time;
    position->z += direction.z * speed * time;
}