#ifndef PLATE
#define PLATE
#pragma once
#include "Object.h"
#include "Scene.h"
#include "../../shaders/phong_frag_glsl.h"
#include "../../shaders/phong_vert_glsl.h"

class Plate final : public Object {

private:
    const std::string planeMesh = "../Resources/trava.obj";
    const std::string planeTexture = "../Resources/trava.bmp";

    ppgso::Shader shader = ppgso::Shader(phong_vert_glsl, phong_frag_glsl);
    ppgso::Mesh mesh = ppgso::Mesh(planeMesh);
    ppgso::Texture texture = ppgso::Texture(ppgso::image::loadBMP(planeTexture));


public:
    Plate(float x, float y, float z);
    ~Plate(){
        shader.~Shader();
        mesh.~Mesh();
        texture.~Texture();
    }

    bool update(Scene &scene, float time);
    void render(Scene &scene);

};
#endif
