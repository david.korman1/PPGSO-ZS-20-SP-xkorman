#include "Background.h"
#include "Scene.h"

Background::Background() {

}

bool Background::update(Scene &scene, float dt) {
    textureOffset.x += dt/300;
    generateModelMatrix();
    return true;

}

void Background::render(Scene &scene) {
    glDepthMask(GL_FALSE);

    shader.use();

    shader.setUniform("TextureOffset", textureOffset);

    shader.setUniform("ModelMatrix", modelMatrix);
    shader.setUniform("ViewMatrix", glm::mat4{1.0f});
    shader.setUniform("ProjectionMatrix", glm::mat4{1.0f});
    shader.setUniform("Texture", texture);
    mesh.render();

    glDepthMask(GL_TRUE);
}