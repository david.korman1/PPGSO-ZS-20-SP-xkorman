#ifndef CUBE
#define CUBE
#pragma once
#include "Object.h"
#include "Scene.h"

class Cube final : public Object {

private:
    const std::string planeMesh = "../Resources/cube.obj";
    const std::string planeTexture = "../Resources/Control.bmp";

    ppgso::Shader shader = ppgso::Shader(texture_vert_glsl, texture_frag_glsl);
    ppgso::Mesh mesh = ppgso::Mesh(planeMesh);
    ppgso::Texture texture = ppgso::Texture(ppgso::image::loadBMP(planeTexture));


public:
    Cube(float x, float y, float z);
    ~Cube(){
        shader.~Shader();
        mesh.~Mesh();
        texture.~Texture();
    }

    bool update(Scene &scene, float time);
    void render(Scene &scene);

};
#endif
