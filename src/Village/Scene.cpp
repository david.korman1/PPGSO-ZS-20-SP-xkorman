#include <algorithm>
#include <thread>
#include <chrono>
#include "Scene.h"
#include "sun.h"
#include "Tree.h"
#include "Leaf.h"
#include "Lamp.h"
#include "Rain.h"


void Scene::render() {
    for (auto& object : objects)
        object->render(*this);

    for (auto& leaf : leafs)
        leaf->render(*this);

    if (!menu) {
        for (auto& shadow : shadows)
            shadow->render(*this);

        for (auto& drop : drops)
            drop->render(*this);
    }
}

void Scene::update(float time, float timeOrigin) {

    camera->update(*this, time);
    auto iter = objects.begin();

    if(menu && (keyboard[GLFW_KEY_ENTER] == GLFW_PRESS || keyboard[GLFW_KEY_SPACE] == GLFW_PRESS)) { // pridany aj medzernik lebo to z nejakeho dovodu zacalo pri entery vyhadzovat chybu
        menu = false;
    }

    while (iter != objects.end())
    {
        Object* pItem = *iter;

        auto sun = dynamic_cast<Sun*>(pItem);
        if (sun) {
            pItem->update(*this, timeOrigin);
            lightDirection = sun->position; // Vypinanie zmeny pozicie svetla
            ++iter;
        } else {
            auto lamp = dynamic_cast<Lamp*>(pItem);
            if (lamp) {
                pItem->update(*this, time);
            } else {
                if (!pItem->update(*this, time)) {
                    iter = objects.erase(iter);
                }
            }
            ++iter;
        }
    }


    auto i = std::begin(leafs);
    while (i != std::end(leafs)) {
        auto leaf = *i;
        if (leaf->position.z > 200 || leaf->position.x > 200)
            i = leafs.erase(i);
        else {
            leaf->update(*this, time);
            ++i;
        }
    }

    auto iterator = std::begin(drops);
    while (iterator != std::end(drops)) {
        auto drop = *iterator;
        if (!drop->update(*this, time))
            iterator = drops.erase(iterator);
        else {
            ++iterator;
        }
    }

    if(!menu) {

        if(keyboard[GLFW_KEY_F] == GLFW_PRESS) {
            if (color_index == colors.size() - 1) {
                color_index = 0;
                lightColor = colors[color_index];
            } else {
                color_index += 1;
                lightColor = colors[color_index];
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(200));
        }

        if(keyboard[GLFW_KEY_L]) {
            float x,z, random;
            Leaf *leaf;
            for (size_t i = 0; i < 10; i++) {
                x = static_cast<float>(rand() % 20 - 10);
                z = static_cast<float>(rand() % 20 - 10);
                random = rand() % 3;
                leaf = new Leaf(x,0,z);
                leaf->scale = {1,1,1};
                leaf->rotation = {-ppgso::PI/2, random, 0};
                leafs.push_back(leaf);
            }
        }

        if(keyboard[GLFW_KEY_R]){
            float x,y,z;
            Rain *drop;
            for (size_t i = 0; i < 50; i++) {
                x = static_cast<float>(rand() % 400 - 200);
                y = static_cast<float>(rand() % 20 + 45);
                z = static_cast<float>(rand() % 400 - 200);
                drop = new Rain(x,y,z);
                drop->scale = {.3,.3,.3};
                drops.push_back(drop);
            }
        }

        for (auto& shadow : shadows) {
            shadow->update(*this, time);
        }
    }
}

void Scene::useCamera(Cam *camera) {
    Scene::camera = std::move(camera);
    Scene::hasCamera = true;
}