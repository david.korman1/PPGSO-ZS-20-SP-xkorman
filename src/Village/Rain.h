#ifndef RAIN
#define RAIN
#pragma once

#include "Object.h"
#include "Scene.h"

class Rain final : public Object {

private:
    const std::string planeMesh = "../Resources/sphere.obj";
    glm::vec3 color = {0., 0.38f, 0.62f};
    ppgso::Shader shader = ppgso::Shader(color_vert_glsl, color_frag_glsl);
    ppgso::Mesh mesh = ppgso::Mesh(planeMesh);


public:
    Rain(float x, float y, float z);
    ~Rain(){
        mesh.~Mesh();
        shader.~Shader();
    };

    bool update(Scene &scene, float time);
    void render(Scene &scene);

};
#endif
